#script per generare i frattali con python
import time, random, matplotlib.pyplot as plt, math
from threading import Thread

#si genera l'immagine poi attraverso una funzioen che dato un segmento ritorna la lista di punti da considerare come frattale, poi rappresenta l'immagine

############################################################################################################################################
#costanti

const_int_tempo = 2

################################################################################################################################################
#funzioni

def calcola_nuovi_punti():
	global gl_lista_punti
	nuova_lista_punti = []
	for i in range(len(gl_lista_punti) -1):
		punto1 = gl_lista_punti[i]
		punto2 = gl_lista_punti[i+1]
		nuovi_punti = gl_funzione_frattale_scelta(punto1=punto1, punto2=punto2)
		if (len(nuova_lista_punti)>0) and (len(nuovi_punti) >0) and (nuova_lista_punti[-1] == nuovi_punti[0]):
			nuova_lista_punti += nuovi_punti[1:]
		else:
			nuova_lista_punti += nuovi_punti[:]
	#aggiorno la lista dei punti
	gl_lista_punti = nuova_lista_punti

def esegui_iterazioni():
	#funzione per eseguire tutte le iterazioni
	time.sleep(const_int_tempo)#aspetto 5 secondi che si carichi tutto
	for i in range(gl_numero_iterazioni):
		print('Iterazione', i+1)
		time.sleep(const_int_tempo)
		calcola_nuovi_punti()
		rappresenta_segmenti()

def rappresenta_segmenti():
	#prende la lista dei segmenti e li rappresenta
	#prima elimino tutte le linee finora prodotte
	global gl_lista_segmenti
	for linea in gl_lista_segmenti:
		linea.remove()
	gl_lista_segmenti = []
	#itero tra tutti i punti
	#ax = plt.gca()
	for i in range(len(gl_lista_punti) -1):
		#prendo i 2 punti
		(x1, y1) = gl_lista_punti[i]
		(x2, y2) = gl_lista_punti[i+1]
		linea = plt.plot( [x1, x2], [y1, y2] )
		gl_lista_segmenti.extend(linea)
	#al termine mostro il grafico
	plt.draw()
	plt.pause(0.001)
	#plt.show()

##################################################################################################################################################
#fuzioni per frattali

def funzione_frattale_triangoli_equilateri(punto1, punto2):
	#funzione che prnede un segmento e restituisce i vercici del triangolo equilatero con tale segmento
	(x1, y1) = punto1
	(x2, y2) = punto2
	lista_punti = [punto1, (0,0), punto2]
	angolo_nuovo = math.atan2(y2-y1, x2-x1) + math.pi/3
	lunghezza_segmento = math.sqrt( (y2-y1)**2 + (x2-x1)**2 )
	x_nuovo = x1 + math.cos(angolo_nuovo)*lunghezza_segmento
	y_nuovo = y1 + math.sin(angolo_nuovo)*lunghezza_segmento
	lista_punti[1] = (x_nuovo, y_nuovo)
	return lista_punti
	
def funzione_frattale_triangoli_emiequilateri(punto1, punto2):
	#funzione che prnede un segmento e restituisce i vercici del triangolo emiequilatero con tale segmento
	(x1, y1) = punto1
	(x2, y2) = punto2
	lista_punti = [punto1, (0,0), punto2]
	angolo_nuovo = math.atan2(y2-y1, x2-x1) + math.pi/6
	lunghezza_segmento = math.sqrt( (y2-y1)**2 + (x2-x1)**2 )/math.sqrt(3)
	x_nuovo = x1 + math.cos(angolo_nuovo)*lunghezza_segmento
	y_nuovo = y1 + math.sin(angolo_nuovo)*lunghezza_segmento
	lista_punti[1] = (x_nuovo, y_nuovo)
	return lista_punti

def funzione_frattale_linea_triangolo(punto1, punto2):
	#funzione che trasforma 1 linea in 2 linee e un triangolo
	(x1, y1) = punto1
	(x2, y2) = punto2
	delta_y = y2-y1
	delta_x = x2-x1
	lunghezza_segmento = math.sqrt( delta_y**2 + delta_x**2 )
	angolo_attuale = math.atan2(delta_y, delta_x)
	lista_punti = [ (x1 + lunghezza_segmento/3*i*math.cos(angolo_attuale), y1 +lunghezza_segmento/3*i*math.sin(angolo_attuale)) for i in range(4) ]
	angolo_nuovo = angolo_attuale + math.pi/3
	(x_triangolo, y_triangolo) = lista_punti[1]
	x_vertice = x_triangolo + lunghezza_segmento/3*math.cos(angolo_nuovo)
	y_vertice = y_triangolo + lunghezza_segmento/3*math.sin(angolo_nuovo)
	lista_punti.insert( 2, (x_vertice, y_vertice) )
	return lista_punti
	
	

#####################################################################################################################################
#main

def main():
	plt.axis('off')
	plt.ion()
	plt.show()
	#rappresento i primi punti
	rappresenta_segmenti()
	#plt.show()
	#faccio un nuovo thread
	#nuovo_thread = Thread(target=esegui_iterazioni, daemon=True)
	#nuovo_thread.run()
	esegui_iterazioni()
	#plt.show()
	
	
####################################################################################################################################
#variabili globali

gl_numero_iterazioni = 3
gl_lista_punti = [ (0,0), (1,0) ]
gl_lista_segmenti = []

gl_lista_funzioni_frattali = [funzione_frattale_triangoli_equilateri, funzione_frattale_triangoli_emiequilateri, funzione_frattale_linea_triangolo]

gl_funzione_frattale_scelta = funzione_frattale_linea_triangolo

#############################################################################################################################################################
#main

if __name__=='__main__':
	main()
