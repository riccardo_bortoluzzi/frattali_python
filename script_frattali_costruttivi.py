#script per generare i frattali costruttivi ovvedo dove non serve eliminare i segmenti precedenti ma se ne aggiungono ad ogni iterazioner di nuovi

import time, random, matplotlib.pyplot as plt, math
from threading import Thread

#####################################################################################################################################################################################################
#calssi
class Punto:
	x=None
	y=None

	def __init__(self, p_x, p_y):
		#mwetodo costruttore
		self.x=p_x
		self.y = p_y

	def __sub__(self, punto2):
		#metodo per eseguire la sottrazione tra punti
		differenza = Punto(p_x=self.x-punto2.x, p_y=self.y-punto2.y) #qui ho messo che la differenza di 2 punto e un oggetto di classe punto per semplicita di memorizzazione delle variabili 
		return differenza
	
	def aggiugi(self, segmento, angolo):
		#metodo per aggiungere un segmento ad un punto
		delta_x = segmento*math.cos(angolo)
		delta_y = segmento*math.sin(angolo)
		nuovo_punto = Punto(p_x=self.x+delta_x, p_y=self.y+delta_y)
		return nuovo_punto

	def angolo(self, punto1):
		#metodo per calcolare l'angolo tra 2 punti
		differenza = self - punto1
		angolo = math.atan2(differenza.y, differenza.x)
		return angolo

	def to_tuple(self):
		#metodo per ritornare il punto sotto forma di tupla
		tupla = (self.x, self.y)
		return tupla

	def lunghezza_segmento(self, punto2):
		#metodo per calcolare la lunghezza di un segmento
		differenaza = self - punto2
		lunghezza = math.sqrt( differenaza.x**2 + differenaza.y**2 )
		return lunghezza

#############################################################################################################################################################################################
#funzioni per le immagini

def calcola_nuova_lista_segmenti(funzione_frattale, lista_segmenti_attuale):
	#funzione per calcolare la nuova lista dei segmenti data quella attuale
	#la lista dei segmenti e' composta da tuple di punti
	nuova_lista = []
	for (punto1, punto2) in lista_segmenti_attuale:
		#calcolo la nuova lisat di punti
		nuova_lista_segmenti = funzione_frattale(punto1=punto1, punto2=punto2)
		#la aggiungo alla precedente
		nuova_lista.extend(nuova_lista_segmenti[:])
	return nuova_lista

def aggiungi_segmenti(lista_segmenti):
	#prende la lista dei segmenti e li rappresenta
	for (punto1, punto2) in lista_segmenti:
		#prendo i 2 punti
		(x1, y1) = punto1.to_tuple()
		(x2, y2) = punto2.to_tuple()
		linea = plt.plot( [x1, x2], [y1, y2] )
	#al termine mostro il grafico
	plt.draw()
	plt.pause(0.001)
	#plt.show()

def esegui_iterazioni(n_iterazioni, segmenti_iniziali, funzione_frattale):
	#funzione per eseguire tutte le iterazioni partendo da una lista di segmenti
	time.sleep(const_int_tempo)#aspetto 5 secondi che si carichi tutto
	lista_segmenti = segmenti_iniziali[:]
	for i in range(n_iterazioni):
		print('Iterazione', i+1)
		time.sleep(const_int_tempo)
		lista_segmenti = calcola_nuova_lista_segmenti(funzione_frattale=funzione_frattale, lista_segmenti_attuale=lista_segmenti)
		aggiungi_segmenti(lista_segmenti=lista_segmenti)

##############################################################################################################################################################################################################################
#funzioni frattali

def frattale_albero_verso_alto(punto1, punto2):
	#funzione per tornare la lisat dei segmenti con la divisione ad albero ed espansione verticale
	#calcolo la lunghezza del segmento
	lunghezza = punto1.lunghezza_segmento(punto2=punto2)
	#calcolo la nuova lunghezza del segmento
	nuova_lunghezza = lunghezza*const_frattale_albero_rapporto_segmento
	#calcolo la differenza di angolo da un segmento e il successivo
	delta_angolo = math.pi/(const_frattale_albero_numero_divisioni+1) #devo mettere +1 perche per avere n segmenti devo avere n+1 divisioni
	#calcolo l'angolo di partenza da cui aggiungere il delta angolo
	#angolo_partenza = punto2.angolo(punto1=punto1) - math.pi/2
	#creo la lista dei prossimi segmenti
	lista_segmenti = [ (punto2, punto2.aggiugi(segmento=nuova_lunghezza, angolo= (i*delta_angolo) )) for i in range(1, const_frattale_albero_numero_divisioni +1) ]
	return lista_segmenti

def frattale_albero_completo(punto1, punto2):
	#funzione per tornare la lisat dei segmenti con la divisione ad albero ed espansione completa
	#calcolo la lunghezza del segmento
	lunghezza = punto1.lunghezza_segmento(punto2=punto2)
	#calcolo la nuova lunghezza del segmento
	nuova_lunghezza = lunghezza*const_frattale_albero_rapporto_segmento
	#calcolo la differenza di angolo da un segmento e il successivo
	delta_angolo = math.pi/(const_frattale_albero_numero_divisioni+1) #devo mettere +1 perche per avere n segmenti devo avere n+1 divisioni
	#calcolo l'angolo di partenza da cui aggiungere il delta angolo
	angolo_partenza = punto2.angolo(punto1=punto1) - math.pi/2
	#creo la lista dei prossimi segmenti
	lista_segmenti = [ (punto2, punto2.aggiugi(segmento=nuova_lunghezza, angolo= (angolo_partenza + i*delta_angolo) )) for i in range(1, const_frattale_albero_numero_divisioni +1) ]
	return lista_segmenti


#####################################################################################################################################
#main

def main():
	plt.axis('off')
	plt.ion()
	plt.show()
	#rappresento i primi punti
	aggiungi_segmenti(lista_segmenti=par_lista_segmenti_iniziali)
	#plt.show()
	#faccio un nuovo thread
	#nuovo_thread = Thread(target=esegui_iterazioni, daemon=True)
	#nuovo_thread.run()
	esegui_iterazioni(n_iterazioni=par_numero_iterazioni, segmenti_iniziali=par_lista_segmenti_iniziali, funzione_frattale=par_funzione_frattale)
	#plt.show()

#######################################################################################################################################################################################################
#costanti (non sarebbe da utilizzare variabili globali)

const_int_tempo = 5
const_frattale_albero_numero_divisioni = 5
const_frattale_albero_rapporto_segmento = 1/2

par_numero_iterazioni = 3
par_lista_segmenti_iniziali = [ (Punto(0,0), Punto(0,2)) ]
par_funzione_frattale = frattale_albero_completo

#############################################################################################################################################################
#main

if __name__=='__main__':
	main()
